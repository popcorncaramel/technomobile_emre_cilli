import React from 'react';
import {IonButton, IonAlert , IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonCard } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab3.css';

const Tab3: React.FC = () => {
  class Recette{
    nom:string="";
    ingredients:Array<string>=[];
    constructor(){
    }
    ChangerNom = (e:any) =>{
      this.nom=e.detail.value!;
    }
    Changeringredients= (e:any) =>{
      this.ingredients=e.detail.value!;
    }

  }
  const recette = new Recette();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Mes Recettes</IonTitle>
        </IonToolbar>
        <IonToolbar>Ajouter une nouvelle recette
          <IonCard>
            <input type="text" id="nom_patisserie" size={100} placeholder="Nom de la patisserie" onChange={(e)=>recette.ChangerNom(e)}>
            </input>
            <input type="text" id="nom_patisserie" size={100} placeholder="Ingredients" onChange={(e)=>recette.Changeringredients(e)}>
            </input>            
          </IonCard>
        </IonToolbar>
        <IonToolbar>
          <IonButton onClick={()=> <IonCard>Hello </IonCard> }expand="block" >Valider la nouvelle recette
            {
              <IonCard title={recette.nom}></IonCard>
            }
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent>
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
