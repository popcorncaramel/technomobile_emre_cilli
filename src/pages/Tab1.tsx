import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonIcon, IonButton } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';

const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
           <IonTitle>
             Fil d'actualité
           </IonTitle>
         </IonToolbar>
      </IonHeader>
    </IonPage>
  );
};

export default Tab1;
