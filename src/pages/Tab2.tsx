import React, {useState} from 'react';
import { IonLabel,IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonSearchbar, IonFooter,IonInput , IonList,IonItem  } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab2.css';

export class tab2 {
  Patisseries:any=[];

  constructor(){
    this.initialisation();
  }
  Filtrer(ev:any){
    this.initialisation();
    const val = ev.detail.value!;
    if(val  && val.trim() !==''){
      this.Patisseries=this.Patisseries.filter((item:any) =>{
        return (item.nom.toLowerCase().indexOf(val.toLowerCase())>-1);
      })
    }

  }
  initialisation()
  {
    this.Patisseries=[{
      "nom":"Paris Brest",
      "niveau":"difficile"
    },{
      "nom":"Macaron",
      "niveau":"difficile"
    },{
      "nom":"Eclair au chocolat",
      "niveau":"difficile"
    },{
      "nom":"Tiramisu",
      "niveau":"moyen"
    },{
      "nom":"Gateau au Yaourt",
      "niveau":"facile"
    },{
      "nom":"Brownie",
      "niveau":"moyen"
    },{
      "nom":"Tarte aux pommes",
      "niveau":"moyen"
    },{
      "nom":"Quatre quart",
      "niveau":"facile"
    },{
      "nom":"Flan",
      "niveau":"moyen"
    },{
      "nom":"Foret Noire",
      "niveau":"difficile"
    }
  ];
  }
}

const Tab2: React.FC = () => {
  const patisseries = new tab2();
  const [searchText, setSearchText] = useState('')
  return (
    <IonPage>
       <IonHeader translucent>
         <IonToolbar>
           <IonTitle>
             Recherche de patisserie
           </IonTitle>
         </IonToolbar>
         <IonToolbar>
           <IonSearchbar onIonChange={e => patisseries.Filtrer(e)} placeholder="Recherche">
           </IonSearchbar>
         </IonToolbar>
       </IonHeader>
       <IonContent>
           <IonList>
           {
             patisseries.Patisseries.map((item:any)=>(
             <IonItem>
               <IonLabel>
                 <h2>{item.nom}</h2>
                   <h3>{item.niveau}</h3>
               </IonLabel>
              </IonItem>))
             }
           </IonList>
       </IonContent>
    </IonPage>
  );
};

export default Tab2;
